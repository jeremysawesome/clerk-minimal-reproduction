# Infinite Redirect Loop When Home URL is current URL

The problem is simple enough to duplicate.

1. Drop the index.html file into a directory where it can be accessed via browser
   (i.e. `$DEVHOST/clerk-minimal-reproduction/`).
2. Adjust the script src and publishable key for your Clerk app.
3. Navigate to **Developers** > **Paths** in your Clerk Dashboard
4. Change your **Application homepage** to `/clerk-minimal-reproduction/`
5. Do the same for all the **User redirects** in **Configure** > **Account Portal** > **Redirects**
6. Log in or sign up for your application via `$DEVHOST/clerk-minimal-reproduction/` in your browser.

Behold the redirect loop.

## Notes

Clerk does output a notice in the console just before it redirects. It tells us that it's redirecting to the Home URL
because it cannot render a SignIn/Up component when a user is already logged in. This isn't helpful if the Home URL *is
the current URL*.

## Expectations

1. Don't redirect to the same url (in the case where the Home URL and current URL are the same)
2. Output a warning or error in the console if the preconditions fail.
3. Don't render the components if the preconditions fail (i.e. the urls are the same).
 